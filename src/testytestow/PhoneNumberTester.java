package testytestow;

import java.util.regex.Matcher;

/**
 * Class containing phone number and basic info about it.
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class PhoneNumberTester {

    /**
     * Enum representing countries.
     */
    public enum Country {
    
        POLAND,
        ANYTHING_ELSE;
    
    }
    
    private String number;
    private Country country;
    
    /**
     * Constructor, that accepts phone number as a parameter.
     * 
     * @param number Phone number in String form.
     */
    public PhoneNumberTester(String number) {
        this.number = number;
        if (number.startsWith("+48")) {
            country = Country.POLAND;
        } else {
            country = Country.ANYTHING_ELSE;
        }
    }
    
    /**
     * Return phone number country based on country prefix.
     * 
     * @return Phone number country.
     */
    public Country getCountry() {
        return country;
    }
    
    /**
     * Method returning phone number in int form (without country prefix).
     * 
     * @return Phone number in int form (without country prefix).
     */
    public int asInt() {
        String number = this.number.replace(" ", "");
        if (number.startsWith("+")) {
            return Integer.parseInt(number.substring(3));
        } else {
            return Integer.parseInt(number);
        }
    }
    
    /**
     * Returns tested phone number.
     * 
     * @return Tested phone number.
     */
    @Override
    public String toString() {
        return number;
    }
    
}
