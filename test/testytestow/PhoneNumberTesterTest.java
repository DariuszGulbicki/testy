package testytestow;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class PhoneNumberTesterTest {

    private PhoneNumberTester tester;
    
    @Test(expected=java.lang.NullPointerException.class)
    public void testConstructorNullParameter() {
        tester = new PhoneNumberTester(null);
    }
    
    @Test
    public void testCountryRecognition() {
        tester =  new PhoneNumberTester("+48384738475");
        assertEquals(PhoneNumberTester.Country.POLAND, tester.getCountry());
        tester =  new PhoneNumberTester("+48837457323");
        assertEquals(PhoneNumberTester.Country.POLAND, tester.getCountry());
        tester =  new PhoneNumberTester("+48384629473");
        assertEquals(PhoneNumberTester.Country.POLAND, tester.getCountry());
        tester =  new PhoneNumberTester("48384738475");
        assertEquals(PhoneNumberTester.Country.ANYTHING_ELSE, tester.getCountry());
        tester =  new PhoneNumberTester("394827374");
        assertEquals(PhoneNumberTester.Country.ANYTHING_ELSE, tester.getCountry());
        tester =  new PhoneNumberTester("384 458 384");
        assertEquals(PhoneNumberTester.Country.ANYTHING_ELSE, tester.getCountry());
    }
    
    @Test
    public void testGettingNumberAsInt() {
        tester = new PhoneNumberTester("+48384928492");
        assertEquals(384928492, tester.asInt());
        tester = new PhoneNumberTester("+48738539");
        assertEquals(738539, tester.asInt());
        tester = new PhoneNumberTester("847593756");
        assertEquals(847593756, tester.asInt());
        tester = new PhoneNumberTester("83793");
        assertEquals(83793, tester.asInt());
        tester = new PhoneNumberTester("387 893 764");
        assertEquals(387893764, tester.asInt());
        tester = new PhoneNumberTester("837 44 5");
        assertEquals(837445, tester.asInt());
    }
    
}
