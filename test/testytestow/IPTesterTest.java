package testytestow;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class IPTesterTest {
    
    @Test(expected=java.lang.NullPointerException.class)
    public void testIPv4NullParameter() {
        IPTester.isIPv4(null);
    }
    
    @Test
    public void testPositiveIPv4() {
        assertEquals(true, IPTester.isIPv4("127.0.0.132131231"));
        assertEquals(true, IPTester.isIPv4("192.168.0.1"));
        assertEquals(true, IPTester.isIPv4("0.0.0.0"));
        assertEquals(true, IPTester.isIPv4("20.129.154.169"));
        assertEquals(true, IPTester.isIPv4("122.243.46.98"));
    }
    
    @Test
    public void testIPv4Letters() {
        assertEquals(false, IPTester.isIPv4("dfjdf"));
        assertEquals(false, IPTester.isIPv4("kfs.fjw.d.wid"));
    }
    
    @Test
    public void testNegativeIPv4() {
        assertEquals(false, IPTester.isIPv4("127001"));
        assertEquals(false, IPTester.isIPv4("74937.384.7.283"));
        assertEquals(false, IPTester.isIPv4("749.482.482"));
        assertEquals(false, IPTester.isIPv4("723.273.9.734.234"));
        assertEquals(false, IPTester.isIPv4("-234.-432.-4.-84"));
    }
    
    @Test(expected=java.lang.NullPointerException.class)
    public void testIPv6NullParameter() {
        IPTester.isIPv6(null);
    }
    
    @Test
    public void testPositiveIPv6() {
        assertEquals(true, IPTester.isIPv6("cb49:d940:dd22:5aa7:725e:d4db:c9ca:e500"));
        assertEquals(true, IPTester.isIPv6("8eb2:8fa6:9ca8:f262:a524:995c:3eb9:208a"));
        assertEquals(true, IPTester.isIPv6("b4e3:a69b:c21d:b8c8:3328:b1f3:9b1a:7df9"));
        assertEquals(true, IPTester.isIPv6("f5f3:7cf5:b841:6ff6:3296:14fe:9c6a:ec8a"));
        assertEquals(true, IPTester.isIPv6("1c16:894a:90c1:3c28:422d:0dbd:3b7f:3eea"));
    }
    
    @Test
    public void testNegativeIPv6() {
        assertEquals(false, IPTester.isIPv6("5d3979ed0b2aa8250f765a8eb4d7ffc3"));
        assertEquals(false, IPTester.isIPv6("a212:c310:5475"));
        assertEquals(false, IPTester.isIPv6("8b0e:85ab:e770:d20c:2cdf:49db:dfcc:"));
        assertEquals(false, IPTester.isIPv6(":1385:8979:ce80:614e:a226:5cf0:126a:47bd"));
        assertEquals(false, IPTester.isIPv6("200e:0d44:3d49:7ef0:d451:22f9:7237:fc5d:3d49:7ef0:d451"));
    }
    
}
